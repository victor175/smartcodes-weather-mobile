import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:weather/models/weatherInfo.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController;
  bool _loading = false;
  String weatherTemp = "";
  String weatherCond = "";
  String cityName = "";
  String wind = "";
  int _currentPage = 0;
  String humidity = "";
  String airPressure = "";
  List<String> regions = ["Dar es salaam", "Dodoma", "Arusha"];
  List<String> images = [
    'assets/images/dar.jpg',
    'assets/images/dodoma.jpg',
    'assets/images/arusha.jpg'
  ];

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    _pageController = PageController();
    // _loading = true;
    getWeatherInfo(regions[_currentPage]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new ExactAssetImage(images[_currentPage]),
            colorFilter:
                ColorFilter.mode(Colors.black.withAlpha(60), BlendMode.darken),
            fit: BoxFit.cover,
          ),
        ),
        // color: colorsBg[_currentPage],
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOutExpo,

        child: BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
          child: Center(
            child: _loading
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Fetching weather data...",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  )
                : Stack(children: [
                    PageView(
                      onPageChanged: (value) {
                        setState(() {
                          _currentPage = value;
                          // _loading = true;
                        });
                        getWeatherInfo(regions[_currentPage]);
                      },
                      controller: _pageController,
                      scrollDirection: Axis.horizontal,
                      children: [
                        _buildPages(context, weatherCond, weatherTemp),
                        _buildPages(context, weatherCond, weatherTemp),
                        _buildPages(context, weatherCond, weatherTemp)
                      ],
                    ),
                    Positioned(
                        bottom: 25,
                        left: 0,
                        right: 0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: _buildIndicator(context, _currentPage),
                        ))
                  ]),
          ),
        ),
      ),
    );
  }

  void getWeatherInfo(String region) async {
    WeatherInfo wf = new WeatherInfo();
    String url = 'http://localhost:3000/weather?region=' + region;
    http.Response response = await http.get(url);
    wf = WeatherInfo.fromJson(jsonDecode(response.body));
    print(response.body);
    setState(() {
      // _loading = false;
      int cel = (wf.temperature - 273.15).round();
      weatherTemp = cel.toString();
      weatherCond = wf.des;
      cityName = wf.cityName;
      wind = wf.wind.toString();
      humidity = wf.humidity.round().toString();
      airPressure = wf.pressure.round().toString();
    });
  }

  List<Widget> _buildIndicator(BuildContext context, int currentIndex) {
    List<Widget> indicators = new List();
    for (int i = 0; i < 3; i++) {
      indicators.add(
        i == currentIndex
            ? _indicator(context, true)
            : _indicator(context, false),
      );
    }
    return indicators;
  }

  Widget _indicator(BuildContext context, bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      height: 10,
      width: isActive ? 30 : 10,
      margin: EdgeInsets.only(left: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: isActive ? Colors.white : Colors.blue.shade200),
    );
  }

  Widget _buildPages(
      BuildContext context, String weatherCond, String weatherTemp) {
    return Stack(
      children: [
        Positioned(
            top: 70,
            left: 20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      weatherTemp,
                      style: TextStyle(fontSize: 80, color: Colors.white),
                    ),
                    Text(
                      " \u00B0",
                      style: TextStyle(fontSize: 30, color: Colors.white),
                      textAlign: TextAlign.start,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    weatherCond,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    cityName,
                    textAlign: TextAlign.end,
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ),
                )
              ],
            )),
        Positioned(
          bottom: 60,
          left: 5,
          right: 5,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildWeatherItem(
                  context, "assets/images/air.png", wind, "km/h", "Wind"),
              _buildWeatherItem(context, "assets/images/humidity.png", humidity,
                  "%", "Humidity"),
              _buildWeatherItem(context, "assets/images/wind.png", airPressure,
                  "hpa", "Air Pressure"),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildWeatherItem(BuildContext context, String imageUrl, String value,
      String siUnit, String title) {
    return Container(
      height: 220,
      width: 120,
      child: Card(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Image.asset(
              imageUrl,
              width: 50,
              height: 50,
              color: Colors.grey[700],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  value,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  siUnit,
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 140,
              height: 50,
              color: Colors.grey[300],
              child: Center(child: Text(title)),
            ),
          ],
        ),
      ),
    );
  }
}
