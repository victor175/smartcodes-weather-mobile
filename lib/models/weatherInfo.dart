import 'package:json_annotation/json_annotation.dart';

part 'weatherInfo.g.dart';

@JsonSerializable()
class WeatherInfo {
  WeatherInfo({
    this.temperature,
    this.description,
    this.cityName,
    this.icon,
    this.wind,
    this.humidity,
    this.pressure,
    this.des,
  });

  final double temperature;
  final String description;
  final String cityName;
  final String icon;
  final double wind;
  final double humidity;
  final double pressure;
  final String des;

  factory WeatherInfo.fromJson(Map<String, dynamic> json) =>
      _$WeatherInfoFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherInfoToJson(this);
}
